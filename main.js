    
    // 1. dohvatiti referencu kontejnera
    const containerField = document.querySelector('.container');
    
    const renderNewArray = (person) => {
    // 2.1. za svaki pojedinacno item u data array-u kreirati novi DOM element tipa DIV
    const newItem = document.createElement('div');
    console.log('div');
    
    // 2.2. dodati klasu da bi povukao css pravila
    newItem.classList.add('container-row');
    
    // 2.3. dodati mu innerHTML -> vezba string templating in js
    newItem.innerHTML = `<i class="fa fa-user picture-item"></i>
                        <p>${person.name}</p>`;
    // 2.4. Dodati ga na kontejner kako bi se renderovao na ekranu
    containerField.appendChild(newItem);
    }
    
    const main = (data) => {
        // const result = jobs.map(company => company.name)
        const newArray  = data.map(user => ({      // 2. proci kroz data array   //cb input => output
            id: user.id,
            name: user.name,
            companyName: user.company.name,
            street : `${user.address.street}, ${user.address.zipcode} ${user.address.city}`
            }));
            newArray.map(persons => renderNewArray (persons));

        console.log(newArray)
    };  

    fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                console.log(data)
                main(data);
    });